<?php

namespace Drupal\userref\Plugin\Filter;

use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * Provides a filter for linking to users.
 *
 * @Filter(
 *   id = "userref",
 *   module = "userref",
 *   title = @Translation("User Reference"),
 *   description = @Translation("Allows you to link to a user by entering an '@' followed by the username."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 * )
 */
class UserRef extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    return new FilterProcessResult(
      preg_replace_callback(
        '/\B\@([A-ZÀ-ÖØ-ÝŽa-zà-öø-ÿž0-9._-]+)\b/',
        function ($matches) {
          $referencedName = $matches[1];

          // First, check for an exact match:
          $user = user_load_by_name($referencedName);
          if ($user) {
            return Link::fromTextAndUrl('@' . $user->getDisplayName(), Url::fromRoute('entity.user.canonical', ['user' => $user->id()]))->toString();
          }

          // Next, try matching without spaces, dots, underscores, and hyphens.
          $query = \Drupal::database()->select('users_field_data', 'u')
            ->fields('u', ['uid', 'name'])
            ->where('REGEXP_REPLACE(u.name, \' |\\\\.|_|-\', \'\') = :referencedName', [':referencedName' => $referencedName]);
          $user = $query->execute()->fetchObject();
          if ($user) {
            return Link::fromTextAndUrl('@' . $user->name, Url::fromRoute('entity.user.canonical', ['user' => $user->uid]))->toString();
          }

          return $matches[0];
        },
        $text
      )
    );
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    return $this->t('Link to a user by entering an "@" followed by the username.');
  }

}
